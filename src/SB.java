/**
 * Sleeping Barbie Algorithm Java Program
 *
 *      Java program to demonstrate algorithm of sleeping barbers problem using Java Thread class
 *
 *         Barber Class and Customer Class implements Runnable interface to create threads and exe
 *
 *
 */

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Constructor class to initialize permits of semaphores
 */
public class SB {
    static Semaphore customers = new Semaphore(0);
    static Semaphore barber = new Semaphore(0);
    static Semaphore mutex = new Semaphore(1); // Mutex lock for barber seat
    static int customerNumber;
    private static int freeSeats = 0; // 0 or 1 to indicate availability of barber seat


    public static void main(String[] args) {
        Bshop shop = new Bshop();
        Barber barber = new Barber(shop);
        CustomerGenerator cg = new CustomerGenerator(shop);

        Thread Barber = new Thread(barber); // Thread for Barber
        Thread customer = new Thread(cg); // Thread for Customers
        customer.start(); // start customer thread - execute run()
        Barber.start(); // start barber thread - execute run()
    } // end of main

    /**
     *
     */
    static class Barber implements Runnable {
        Bshop shop;

        public Barber(Bshop shop) {
            this.shop = shop;
        }
        public void run() {
            try
            {
                Thread.sleep(10000);
            }
            catch(InterruptedException iex)
            {
                iex.printStackTrace();
            }
            System.out.println("\nBarber started..");
            while(true)
            {
                shop.cutHair();
            }
        }
    }

    /**
     * Customer Class
     */
    static class Customer implements Runnable {
        String name;
        int customerNumber;
        Date inTime;
        Bshop shop;

        public Customer(Bshop shop)
        {
            this.customerNumber = SB.customerNumber;
            this.shop = shop;
        }

        public String getName() {
            return name;
        }

        public Date getInTime() {
            return inTime;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setInTime(Date inTime) {
            this.inTime = inTime;
        }

        public int getCustomerNumber() {
            return customerNumber;
        }

        public void setCustomerNumber(int customerNumber) {
            this.customerNumber = customerNumber;
        }

        public void run() {
            goForHairCut(); // Shared resource
        }
        private synchronized void goForHairCut()
        {
            shop.add(this);
        }
    } // end of Customer

    static class CustomerGenerator implements Runnable {
        Bshop shop;

        public CustomerGenerator(Bshop shop) {
            this.shop = shop;
        }

        public void run() {
            while(true) {
                Customer customer = new Customer(shop);
                customer.setInTime(new Date());
                customer.setCustomerNumber(++customerNumber);
                Thread customerThread = new Thread(customer);
                customer.setName(generateName());
                customerThread.start();

                try {
                    TimeUnit.SECONDS.sleep((long)(Math.random()*10));
                }
                catch(InterruptedException iex) {
                    iex.printStackTrace();
                }
            }
        }

        /**
         * Function that generates random customer names
         * @return name
         */
        public String generateName() {
            String [] k = { "a", "e", "i", "o", "u" };
            String [] mk = { "b", "c", "d", "f", "g", "h", "j", "k",
                    "l", "m", "n", "p", "r", "s", "t", "v", "w", "z" };

            String newName = "";
            int r;

            for( int i=1 ; i<=5 ; i++ ) {

                if( i%2 == 1 ) {
                    r = (int)Math.round( Math.random()* (mk.length-1) );
                    newName += mk[r];
                }else{
                    r = (int)Math.round( Math.random()* (k.length-1) );
                    newName += k[r];
                }

                if( i==1 ) newName = newName.toUpperCase();

            }

            return newName;
        }
    }

    /**
     * Barber shop class
     */
    static class Bshop {
        private int chair;
        final List<Customer> listCustomer; // store customer list

        public Bshop() {
            this.chair = 4;
            listCustomer = new LinkedList<>();
        }

        /*
            Shared resource of customers
         */
        public void cutHair() {
            Customer customer;
            System.out.println("\nBarber waiting for lock.");
            while (listCustomer.size() == 0) {
                System.out.println("Barber is waiting for customer.");
                try {
                    customers.acquire(); // Check customer semaphore; if > 0, can be accessed
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                try {
                    mutex.acquire(); // lock barber seat
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } // end of while
            freeSeats++; // increment barber seat by 1, means available.

            System.out.println("Barber found a customer in the queue.");
            customer = (Customer)((LinkedList<?>)listCustomer).poll(); // pop first element in the list
            long duration = 0; // holds duration of haircut

            try {
                System.out.printf("Cutting hair of Customer [%d]: " + customer.getName() + "\n", customer.getCustomerNumber());
                duration = (long)(Math.random() * 10);
                TimeUnit.SECONDS.sleep(duration);
            }
            catch(InterruptedException iex) {
                iex.printStackTrace();
            }
            System.out.printf("Completed Cutting hair of Customer [%d] : " + customer.getName() + " in " + duration +
                    " seconds.\n", customer.getCustomerNumber());
            barber.release(); // Release lock
            mutex.release(); // Release lock for barber seat (mutex)
            chair++;
        }

        /**
         * Customer semaphore
         * @param customer
         */
        public void add(Customer customer) {
            System.out.printf("\n\t\t Number of chairs in the waiting room: [%d]", 3 );
            System.out.printf("\nNew Customer [%d] : " + customer.getName()+ " entering the shop at " + customer.getInTime() + "\n"
                    , customer.getCustomerNumber());

            System.out.printf("\n\t\t Number of customers in the waiting room: %d%n", listCustomer.size());
            if (listCustomer.size() == 3) {
                System.out.println("No chair available for customer " + customer.getName());
                System.out.printf("Customer [%d] " + customer.getName()+ " leaves the shop...\n", customer.getCustomerNumber());
                return;
            }

            else {
                ((LinkedList<Customer>) listCustomer).offer(customer); // adds new customer at the tail of the list
                System.out.printf("Customer [%d]: " + customer.getName() + " got the chair.\n", customer.getCustomerNumber());
                chair--;
            }

            if (freeSeats > 0) {
                try {
                    mutex.acquire();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                freeSeats--; // Set barber seat to 0 - customer having a haircut
                customers.release(); // release customer permit
                mutex.release(); // release barber seat lock

                try {
                    barber.acquire(); // Acquire permit, if < 0; Customer will wait.
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
            mutex.release(); // Release mutex lock
        }
    }
} // end of SB class