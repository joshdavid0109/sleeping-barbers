/**
 * Sleeping Barbie Algorithm Java Implementation
 * <p>
 * Imagine a hypothetical barbershop with one barber, one barber chair, and a waiting room with n chairs (n may be 0)
 * for waiting customers.
 * <p>
 * The following rules apply:
 * 1. If there are no customers, the barber falls asleep in the chair
 * <p>
 * 2. A customer must wake the barber if he is asleep
 * <p>
 * 3. If a customer arrives while the barber is working, the customer leaves if all chairs are occupied and sits in an
 * empty chair
 *    if it's available
 * <p>
 * 4. When the barber finishes a haircut, he inspects the waiting room to see if there are any waiting customers and
 * falls asleep if there are none
 * <p>
 * Solution:
 *      Three Semaphores (One for barber, one for customer, and a mutex lock to provide mutual exclusion)
 * <p>
 *      In java, [acquire() and release()] methods are operations that locks and releases permits for shared resources
 *      (Barber seat in this case)
 *      
 *              Permits are used to specify number of threads that can access a shared resource at any one time.
 *              If permit is one, only one thread
 *              can access the shared resource at any one time.
 */

import java.util.concurrent.Semaphore;

public class SleepingBarbie {
    /*
        Variable declaration
     */
    Semaphore customers; // Counts number of customers in the waiting room
    Semaphore barber; // 0 or 1 to check if barber is idle or working
    Semaphore mutex; // Mutex lock for barber seat
    private int freeSeats; // Number of customers counter

    /**
     * Constructor class to initialize values of Semaphores
     */
    public SleepingBarbie() {
        this.customers = new Semaphore(0); // there will be no threads that can access these
        this.barber = new Semaphore(0);
        this.mutex = new Semaphore(1);
        this.freeSeats = 5;
    }

    /**
     * Barber class
     */
    class Barber {
        public void run(){
            while (true) {
                try {
                    customers.acquire(); // Acquire for customer, if none/ if permit is < 1; go to sleep
                    System.out.println("Barber is asleep (Waiting for customer).");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                // Barber is awake
                try {
                    mutex.acquire(); // Get access to number of available seats, else goes back to sleep
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }  // if barber gets a permit, increment the counter variable or the number of available chair by 1
                System.out.println("Barber chair available.");
                freeSeats++; // chair available
                barber.release(); // Signal that barber is ready to cut
                mutex.release(); // release lock for seat
                // In which case the customer will have a haircut
                System.out.println("Customer having a haircut");
            }
        } // end of while
    } // end of Barber class

    /**
     * Customer class
     */
    class Customer{
        public void run() {
            while (true) { // infinite loop to generate multiple customers
                try {
                    mutex.acquire(); // Try to get access to waiting room chairs
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                if (freeSeats > 0) {
                    System.out.println("Customer takes seat");
                    freeSeats--; // Decrement number of available chairs
                    System.out.println("Notifies the barber");
                    customers.release(); // notify barber - release lock
                    mutex.release(); // release mutex lock for seat

                    try {
                        barber.acquire(); // check if barber is idle or working; wait if barber is busy
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Customer having a haircut");
                    // customer having a haircut
                } else // No available chair in the waiting room
                        // Customer leaves the shop and releases mutex
                    mutex.release();
            } // end of while
        } // end of run
    } // end of Customer class
} // end of Sleeping Barber class
